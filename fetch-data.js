const request = require('request');
const fs = require('fs');

const baseUrl = 'https://cdn-data-eiq-kopla-integration.igusdev.igus.de';
const limit = 10000;

const kettenSchema = '5';
const kettenPath = 'ketten';

const aesSchema = '4';
const aesPath = 'anschlusselemente';

const leitungSchema = '5';
const leitungPath = 'leitungen';

const iaSchema = '5';
const iaPath = 'innenaufteilungselemente';

const zugentlastungenSchema = '6';
const zugentlastungenPath = 'zugentlastungen';

const verpackungSchema = '8';
const verpackungPath = 'verpackungen';

const rinnenSchema = '5';
const rinnenPath = 'rinnen';

const montagesetsSchema = '5';
const montagesetsPath = 'montagesets';

const leitungsSerienSchema = '4';
const leitungsSerienPath = 'leitungsSerien';

fetchKetten();
fetchAEs();
fetchLeitungen();
fetchIAs();
fetchZugentlastungen();
fetchVerpackungen();
fetchRinnen();
fetchMontageSets();

fetchLeitungsSerien();

//
// request
//

function _request(article, schema, limit, searchAfter) {
    const url = `${baseUrl}/${article}?schemaVersion=${schema}&limit=${limit}${
        searchAfter !== -1 ? `&exclusiveStartKey={"searchAfter":["${searchAfter}"]}` : ''
    }`;
    console.log('Request:', url);

    return new Promise((resolve, reject) => {
        request.get({
            url: encodeURI(url),
            json: true,
            headers: {
                'Kopla-Service-ID': 'd1bd2f77-53ec-4225-aa3d-d97ff554f1e5'
            }
        }, (error, response, body) => {
            if (error || response.statusCode !== 200) {
                console.error(response.statusCode, error, body);
                reject(response.statusCode);
                return;
            }

            resolve(body);
        });
    });
}

//
// Ketten
//

function fetchKetten() {
    let ketten = [];
    let searchAfter = -1;

    const responseFn = (body) => {
        ketten = ketten.concat(body.Items);

        if (body.Items.length === limit) {
            searchAfter = body.LastEvaluatedKey.searchAfter[0];
            _request(kettenPath, kettenSchema, limit, searchAfter).then(responseFn);
        } else {
            console.log('Number of Ketten:', ketten.length);
            fs.writeFileSync('ketten.js', `module.exports=${JSON.stringify(ketten)};`);
        }
    };

    _request(kettenPath, kettenSchema, limit, searchAfter).then(responseFn);
}

//
// AEs
//

function fetchAEs() {
    let aes = [];
    let searchAfter = -1;

    const responseFn = (body) => {
        aes = aes.concat(body.Items);

        if (body.Items.length === limit) {
            searchAfter = body.LastEvaluatedKey.searchAfter[0];
            _request(aesPath, aesSchema, limit, searchAfter).then(responseFn);
        } else {
            console.log('Number of AEs:', aes.length);
            fs.writeFileSync('aes.js', `module.exports=${JSON.stringify(aes)};`);
        }
    };

    _request(aesPath, aesSchema, limit, searchAfter).then(responseFn);
}

//
// Leitungen
//

function fetchLeitungen() {
    let leitungen = [];
    let searchAfter = -1;

    const responseFn = (body) => {
        leitungen = leitungen.concat(body.Items);

        if (body.Items.length === limit) {
            searchAfter = body.LastEvaluatedKey.searchAfter[0];
            _request(leitungPath, leitungSchema, limit, searchAfter).then(responseFn);
        } else {
            console.log('Number of Leitungen:', leitungen.length);
            fs.writeFileSync('leitungen.js', `module.exports=${JSON.stringify(leitungen)};`);
        }
    };

    _request(leitungPath, leitungSchema, limit, searchAfter).then(responseFn);
}

//
// IAs
//

function fetchIAs() {
    let ias = [];
    let searchAfter = -1;

    const responseFn = (body) => {
        ias = ias.concat(body.Items);

        if (body.Items.length === limit) {
            searchAfter = body.LastEvaluatedKey.searchAfter[0];
            _request(iaPath, iaSchema, limit, searchAfter).then(responseFn);
        } else {
            console.log('Number of IAs:', ias.length);
            fs.writeFileSync('ias.js', `module.exports=${JSON.stringify(ias)};`);
        }
    };

    _request(iaPath, iaSchema, limit, searchAfter).then(responseFn);
}

//
// Zugentlastungen
//

function fetchZugentlastungen() {
    let zugentlastungen = [];
    let searchAfter = -1;

    const responseFn = (body) => {
        zugentlastungen = zugentlastungen.concat(body.Items);

        if (body.Items.length === limit) {
            searchAfter = body.LastEvaluatedKey.searchAfter[0];
            _request(zugentlastungenPath, zugentlastungenSchema, limit, searchAfter).then(responseFn);
        } else {
            console.log('Number of Zugentlastungen:', zugentlastungen.length);
            fs.writeFileSync('zugentlastungen.js', `module.exports=${JSON.stringify(zugentlastungen)};`);
        }
    };

    _request(zugentlastungenPath, zugentlastungenSchema, limit, searchAfter).then(responseFn);
}

//
// Verpackungen
//

function fetchVerpackungen() {
    let verpackungen = [];
    let searchAfter = -1;

    const responseFn = (body) => {
        verpackungen = verpackungen.concat(body.Items);

        if (body.Items.length === limit) {
            searchAfter = body.LastEvaluatedKey.searchAfter[0];
            _request(verpackungPath, verpackungSchema, limit, searchAfter).then(responseFn);
        } else {
            console.log('Number of Verpackungen:', verpackungen.length);
            fs.writeFileSync('verpackungen.js', `module.exports=${JSON.stringify(verpackungen)};`);
        }
    };

    _request(verpackungPath, verpackungSchema, limit, searchAfter).then(responseFn);
}

//
// Rinnen
//

function fetchRinnen() {
    let rinnen = [];
    let searchAfter = -1;

    const responseFn = (body) => {
        rinnen = rinnen.concat(body.Items);

        if (body.Items.length === limit) {
            searchAfter = body.LastEvaluatedKey.searchAfter[0];
            _request(rinnenPath, rinnenSchema, limit, searchAfter).then(responseFn);
        } else {
            console.log('Number of Rinnen:', rinnen.length);
            fs.writeFileSync('rinnen.js', `module.exports=${JSON.stringify(rinnen)};`);
        }
    };

    _request(rinnenPath, rinnenSchema, limit, searchAfter).then(responseFn);
}

//
// Montage Sets
//

function fetchMontageSets() {
    let montageSets = [];
    let searchAfter = -1;

    const responseFn = (body) => {
        montageSets = montageSets.concat(body.Items);

        if (body.Items.length === limit) {
            searchAfter = body.LastEvaluatedKey.searchAfter[0];
            _request(montagesetsPath, montagesetsSchema, limit, searchAfter).then(responseFn);
        } else {
            console.log('Number of Montage Sets:', montageSets.length);
            fs.writeFileSync('montageSets.js', `module.exports=${JSON.stringify(montageSets)};`);
        }
    };

    _request(montagesetsPath, montagesetsSchema, limit, searchAfter).then(responseFn);
}

//
// Leitungsserien
//

function fetchLeitungsSerien() {
    let leitungsSerien = [];
    let searchAfter = -1;

    const responseFn = (body) => {
        leitungsSerien = leitungsSerien.concat(body.Items);

        if (body.Items.length === limit) {
            searchAfter = body.LastEvaluatedKey.searchAfter[0];
            _request(leitungsSerienPath, leitungsSerienSchema, limit, searchAfter).then(responseFn);
        } else {
            console.log('Number of Leitungs Serien:', leitungsSerien.length);
            fs.writeFileSync('leitungsSerien.js', `module.exports=${JSON.stringify(leitungsSerien)};`);
        }
    };

    _request(leitungsSerienPath, leitungsSerienSchema, limit, searchAfter).then(responseFn);
}